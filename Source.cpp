#include <iostream> 
#include <iostream> 
#include <string> 
#include <vector> 
#include <algorithm>
//#include <windows.h>
using namespace std;
vector<int> z_function(string s)
{
	int n = (int)s.length();
	vector<int> z(n);
	for (int i = 1, l = 0, r = 0; i < n; ++i) {
		if (i <= r) {
			z[i] = min(r - i + 1, z[i - l]);
		}
		while (i + z[i] < n && s[z[i]] == s[i + z[i]]) {
			++z[i];
		}
		if (i + z[i] - 1 > r) {
			l = i, r = i + z[i] - 1;
		}
	}
	return z;
}

int main()
{
	int n, p;
	string Pattern, Text;
	getline(cin, Pattern);
	std::cin >> Text;
	n = Pattern.length();
	p = Text.length();
	string P_Txt = Pattern + '#' + Text;
	vector <int> real_z = z_function(P_Txt);
	for (int i = 0; i < p + n + 1; ++i) {
		if (real_z[i] == Pattern.length())
			cout << i - n - 1 << " ";
	}
	//Sleep(2000);
	return 0;
}