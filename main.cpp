#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

#define _USE_MATH_DEFINES
const double d = 1e-11;


using namespace std;
struct Point{
    double x;
    double y;
};


//Definition of operators with coordinates
Point operator + (const Point &point1, const Point &point2){

    Point sumanswer;
    sumanswer.x = point1.x + point2.x;
    sumanswer.y = point1.y + point2.y;
    return sumanswer;
}


Point operator - (const Point &point1, const Point &point2){

    Point minusanswer;
    minusanswer.x = point1.x - point2.x;
    minusanswer.y = point1.y - point2.y;
    return minusanswer;
}


double  Length(Point point) {

    return  (sqrt( point.x*point.x + point.y*point.y ));

}

bool operator == (const Point &FirstPoint, const Point &SecondPoint){

    return (FirstPoint.y == SecondPoint.y && FirstPoint.x == SecondPoint.x);
}

double  ScalarMult(const Point &p1, const Point &p2) {
    return (p1.x*p2.x + p1.y*p2.y);
}

double MultOfVect (const Point &p1, const Point &p2) {
    return p1.x * p2.y - p2.x * p1.y;
}

double angle (Point &point1, Point &point2)//Angle for MinkSum
{
    Point Horizon = {1,0};
    double Cos_of_angle = ScalarMult((point2 - point1) , ( Horizon ) )/ (Length(point2 - point1) );
    double Angle_forMink = acos(Cos_of_angle) ;

    if ( ((point2 - point1).y >= 0) == true)
        return Angle_forMink;
    else
        return 2*M_PI - Angle_forMink;
}

bool Is_OnTheEdge(const Point &p1, const Point &p2, const Point &point) {

    double sum_of_modules = Length(point - p1) + Length(point - p2);
    return fabs(sum_of_modules + ScalarMult((p1 - point) , (p2 - point))) < d;
}


bool IsInside(const Point &point, const int Num, const vector<Point> &Figure) {

    bool in_f = true;

    double MultVect_1 = MultOfVect((point - Figure[0]), (Figure[1] - Figure[0]));
    if (fabs(MultVect_1) < d)
        in_f = false;

    for (int i = 0; i < Num - 1; ++i) {
        double Curr_MultVect = MultOfVect((point - Figure[i]) , (Figure[i + 1] - Figure[i]));
        if ( (MultVect_1 * Curr_MultVect < -d)  || (fabs(Curr_MultVect) < d))
            in_f = false;
    }

    double Last_VectMult = MultOfVect((point - Figure[Num - 1]) , (Figure[0] - Figure[Num - 1]));
    if ((MultVect_1 * Last_VectMult < -d)  || (fabs(Last_VectMult) < d))
        in_f = false;

    bool on_f = false;

    for (int i = 0; i < Num - 1; ++i) {
        if (Is_OnTheEdge(Figure[i], Figure[i + 1], point) ) {
            on_f = true;
            break;
        }
    }

    if (Is_OnTheEdge(Figure[0], Figure[Num - 1], point) ) { on_f = true; }
    if (in_f == true){
        return in_f;
    }
    else if(on_f == true) {
        return on_f;
    }
}

vector<Point> MiinkSum(const int num_1, vector<Point> &Figure_1, const int num_2, vector<Point> &Figure_2) {

    vector <Point> result;
    int index_1 = 0;
    int index_2 = 0;

    do {

        result.push_back(Figure_1[index_1] + Figure_2[index_2]);

        if (angle(Figure_1[index_1], Figure_1[index_1 + 1]) < angle(Figure_2[index_2], Figure_2[index_2 + 1])) {

            index_1 += 1;
            if (index_1 == num_1) {
                while (index_2 < num_2) {
                    result.push_back(Figure_1[index_1] + Figure_2[index_2]);
                    index_2 += 1;
                }
            }
        } else if (angle(Figure_1[index_1], Figure_1[index_1 + 1]) > angle(Figure_2[index_2], Figure_2[index_2 + 1])) {

            index_2 += 1;
            if (index_2 == num_2) {
                while (index_1 < num_1) {
                    result.push_back(Figure_1[index_1] + Figure_2[index_2]);
                    index_1 += 1;
                }
            }
        } else {
            if (index_1 != num_1 && index_2 != num_2) { ++index_1, ++index_2; }

            if (index_1 == num_1) {
                while (index_2 < num_2) {
                    result.push_back(Figure_1[index_1] + Figure_2[index_2]);
                    index_2 += 1;
                }
            }
            if (index_2 == num_2) {
                while (index_1 < num_1) {
                    result.push_back(Figure_1[index_1] + Figure_2[index_2]);
                    ++index_1;
                }
            }
        }
    } while (index_1 < num_1 || index_2 < num_2);
    return result;
}

void Change_Direction(const int n, vector<Point> &Figure){

    for (int i = 0; i < n; ++i){
        Figure[i].x = -Figure[i].x;
        Figure[i].y = -Figure[i].y;
    }
}


int FindStart(vector <Point> &Figure){
    int NumbofStrt = 0;
    Point Start = Figure[0];
    for (int i = 1; i < Figure.size();++i){
        if (Figure[i].y < Start.y){
            Start = Figure[i];
            NumbofStrt=i;

        }
        if ((Figure[i].x<Start.x) && (Figure[i].y == Start.y)){
            Start = Figure[i];
            NumbofStrt=i;
        }
    }
    return NumbofStrt;
}


vector <Point> Change_order(vector <Point> &CommonVect){
    Point MainPoint = {0,0};
    vector <Point> Figure(CommonVect.size(), MainPoint);
    Figure[0] = CommonVect[0];
    for (int i = 1; i < CommonVect.size(); ++i){
        Figure[i] = CommonVect[CommonVect.size() - i];
    }
    vector <Point> cycle_CommonVect = Figure;
    int Start_index_1 = FindStart( Figure);

    if (Start_index_1 != 0)
        rotate(Figure.begin(), Figure.begin() + Start_index_1, Figure.end());
    return Figure;
}


int main() {

    int num_1;
    cin >> num_1;

    vector <Point> CommonVect_1;
    Point a;
    for (int i = 0; i < num_1; ++i){
        cin >> a.x >> a.y;
        CommonVect_1.push_back(a);
    }

    vector <Point> Figure_1 = Change_order(CommonVect_1);

    int num_2;
    cin >> num_2;

    vector <Point> CommonVect_2;
    for (int i = 0; i < num_2; ++i){
        cin >> a.x >> a.y;
        CommonVect_2.push_back(a);
    }
    Change_Direction(CommonVect_2.size(), CommonVect_2);
    vector <Point> Figure_2 = Change_order(CommonVect_2);

    Figure_1.push_back(Figure_1[0]);
    Figure_2.push_back(Figure_2[0]);
    Figure_1.push_back(Figure_1[1]);
    Figure_2.push_back(Figure_2[1]);

    vector <Point> MinkSum_result = MiinkSum(num_1, Figure_1, num_2, Figure_2);

    bool Result;
    Result = IsInside({0, 0}, MinkSum_result.size(), MinkSum_result);
    if (Result == true){
        cout<<"YES";
    }
    else{
        cout << "NO";
    }
    //system("pause");
    return 0;
}